const puppeteer = require('puppeteer');
 let browser
 let page
 const firstName = 'Test'
 const lastName = 'Automation'
 const email = Date.now() + '@automation.com'
 const password = 'NKkn*8h98fs98hvc&&('
 const website =
     'https://sportland.lv/'
 /*
     'https://demo-sportland.readymage.com/'
     'https://demo-sportland-ee.readymage.com/'
     'https://demo-sportland-lt.readymage.com/'
     'https://demo-sportland-lv.readymage.com/'
     'https://sportland.com/'
     'https://sportland.ee/'
     'https://sportland.lt/'
     'https://sportland.lv/'
 */
 before(async () => {
     browser = await puppeteer.launch({
         headless: false,
    	args:[
       		'--start-fullscreen'
    	]
     })
     page = await browser.newPage()
     await page.setViewport({ width: 1920, height: 1080});
 })
 describe('User Registration', function() {
     this.timeout(5000)
     it(`Having an access to storefront : ${website}`, async () => {
         await page.goto(website)
         await page.waitForSelector('.Header-Button.Header-Button_type_account')
     }).timeout(10000)
     it('Clicking Register/Sign In button', async () => {
         await page.click('.Header-Button.Header-Button_type_account')
     })
     it('Sign in to your account drop-down appearing', async () => {
         await page.waitForSelector('.Button.Button_isHollow')
     })
     it('Clicking CREATE AN ACCOUNT button', async () => {
         await page.click('.Button.Button_isHollow')
     })
     it('Create new account drop-down appearing', async () => {
         await page.waitForSelector('#termsAndConditions')
     })
     it('Filling First Name field with valid info', async () => {
         await page.type('#firstname', firstName)
     })
     it('Filling Last Name field with valid info', async () => {
         await page.type('#lastname', lastName)
     })
     it('Filling Email field with valid info', async () => {
         await page.type('#email', email)
         console.log('Registration e-mail: ' + email)
     })
     it('Filling Password field with valid info', async () => {
         await page.type('#password', password)
     })
     it('Filling Confirm password field with valid info', async () => {
         await page.type('#confirm_password', password)
         await page.waitForSelector('.MyAccountOverlay-Action > .Form > .MyAccount8Overlay-TermsAndConditions > .Field > label')
     }).timeout(3000)
     it('Checking I agree to terms and conditions checkbox', async () => {
         await page.click('.MyAccountOverlay-Action > .Form > .MyAccountOverlay-TermsAndConditions > .Field > label')
         await page.waitForSelector('.Overlay > .MyAccountOverlay-Action > .Form > .MyAccountOverlay-Buttons > .Button')
     })
     it('Clicking on SIGN UP button', async () => {
          await page.click('.Overlay > .MyAccountOverlay-Action > .Form > .MyAccountOverlay-Buttons > .Button')
     })
     it('Redirecting to My Account Dashboard page', async () => {
         await page.waitForSelector('.MyAccountDashboard-CustomerData')
     })
 })
 after(async () => {
     await browser.close()
 })