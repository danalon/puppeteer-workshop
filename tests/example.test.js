const puppeteer = require('puppeteer');

let browser
let page

before(async () => {
    browser = await puppeteer.launch({
        headless: true,
        args:[
            '--start-fullscreen'
        ]
    })
    page = await browser.newPage()
    await page.setViewport({ width: 1920, height: 1080})
})

describe('Lesson test', function() {

    it('Our first test step', async () => {
        await page.goto('https://www.sportland.com/')
        await page.waitForSelector('.Header-Button.Header-Button_type_account')
    }).timeout(10000)

    it('Our second test step', async () => {
        await page.click('.Header-Button.Header-Button_type_account')
    }).timeout(3000)

    it('Our third test step', async () => {
        await page.waitForSelector('.Button.Button_isHollow')



        await page.waitForTimeout(3000)
    }).timeout(10000)

})

after(async () => {
    await browser.close()
})